import os
import dash
import dash_core_components as dcc
import dash_html_components as html

DASHBOARD_PATH = os.path.abspath(os.path.dirname(__file__))

external_stylesheets = [
    {
        'rel': "stylesheet",
        'href': "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",
        'integrity': "sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh",
        'crossorigin': "anonymous"
    }
]

app = dash.Dash(
    __name__,
    external_stylesheets=external_stylesheets,
    )

def generte_plot():
    return dcc.Graph(
            figure={
                'data': [
                    {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                    {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
                ],
                'layout': {
                    'title': 'Dash Data Visualization'
                }
            }
        )

app.layout = html.Div(
[
    html.Nav(
        html.Div(
            [
                html.A('Option1', className="nav-item nav-link", href='https://www.google.ch'),
                html.A('Option2', className="nav-item nav-link", href='https://www.google.ch'),
                html.A('Option3', className="nav-item nav-link", href='https://www.google.ch'),
            ], 
            className='navbar-nav'
        ), 
        className="navbar sticky-top navbar-expand-lg navbar-dark bg-dark"
    ),
    html.Div(
        [
            html.H1('Hello Dash'),
            html.P('Dash: A web application framework for Python.'),
        ]
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='row',
        style={'margin-bottom': '30px'},
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='row',
        style={'margin-bottom': '30px'}
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='row',
        style={'margin-bottom': '30px'}
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='row',
        style={'margin-bottom': '30px'}
    ),      
],
className='dalmo jumbotron container'
)

if __name__ == '__main__':
    app.run_server(debug=True)